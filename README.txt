SUMMARY - YouTube Field
-------------
Livechatoo module provide a livechatoo.com service into drupal block.


REQUIREMENTS
-------------
All dependencies of this module are enabled by default in Drupal 7.x.


INSTALLATION
-------------
Install this module as usual. Please see
http://drupal.org/documentation/install/modules-themes/modules-7


USAGE
-------------
1. Set Livechatoo block to any region.
2. Set Livechatoo account name "demo" for test use.
3. Save configuration.
