(function ($) {

    Drupal.behaviors.livechatoo = {
        attach: function (context, settings) {
            livechatooCmd = function () {
                livechatoo.embed.init({
                    account: settings.livechatoo.account,
                    lang: settings.livechatoo.language,
                    side: settings.livechatoo.side,
                });
            };

            var l = document.createElement('script');
            l.type = 'text/javascript';
            l.async = !0;
            l.src = 'http' + (document.location.protocol == 'https:' ? 's' : '') + '://app.livechatoo.com/js/web.min.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(l, s);

        }
    };

})(jQuery);
